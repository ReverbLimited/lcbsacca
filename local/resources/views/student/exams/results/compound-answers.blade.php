<?php $questions = json_decode($question->
answers);
$correct_answers    = json_decode($question->correct_answers);
$j=0;

//$leftdata = $answers->left;
//$rightdata = $answers->right;


?>
<div class="match-questions row">


    <div class="col-md-12">

        @foreach($questions->child as $q)
            <div class="select-answer">
                <label style="margin-left: 20px">  {{$q->title}}</label>
                <br>
                <ul class="row list-style-none">
                    <?php $i=1; ?>
                    @foreach($q->options as $option)
                        <?php $rand_no = mt_rand(1,1000000);
                            $checked='';
                            $correct_answer_class = '';
                            ?>

                            @if($user_answers[$j]==$i)
                                <?php $checked='checked'; ?>
                            @endif
                            @if($correct_answers[$j]->answer==$i)
                                <?php $correct_answer_class = 'correct-answer'; ?>
                            @endif

                        <li class="col-md-6 {{$correct_answer_class}}">
                            <input id="{{ $option}}_{{$rand_no}}" value="{{$i}}" name="{{$question->id}}[]" type="checkbox" {{$checked}} disabled />
                                <label for="{{ $option}}_{{$rand_no}}">
                    <span class="fa-stack checkbox-button">
                        <i class="mdi mdi-check active">
                        </i>
                    </span>
                                    {{$option}}
                                </label>
                                </input>






                        </li>
                        <?php $i++;?>
                    @endforeach





                </ul>

            </div>

            <br>
            <?php $j++?>
        @endforeach

    </div>
</div>
<?php
//$leftdata = $answers->left;
//$rightdata = $answers->right;


?>
<div class="match-questions row">


    <div class="col-md-12">

        <fieldset class="form-group">
            {{ Form::label($question->id.'[0]', 'Answer') }}
            {{ Form::textarea($question->id.'[0]', $value = $user_answers[0], $attributes = array('class'=>'form-control ckeditor', 'placeholder' => 'Your answer', 'rows' => '5','id'=>'studentanswer')) }}
        </fieldset>

    </div>
</div>
<?php $questions = json_decode($question->
answers);

//$leftdata = $answers->left;
//$rightdata = $answers->right;
 

?>
<div class="match-questions row">


    <div class="col-md-12">

        @foreach($questions->child as $q)
        <div class="select-answer">
            <label style="margin-left: 20px">  {{$q->title}}</label>
            <br>
            <ul class="row list-style-none">
                <?php $i=1; ?>
                @foreach($q->options as $option)
                    <?php $rand_no = mt_rand(1,1000000); ?>
                <li class="col-md-6">
                    <input id="{{ $option}}_{{$rand_no}}" value="{{$i++}}" name="{{$question->id}}[]" type="checkbox"/>

                    <label for="{{ $option}}_{{$rand_no}}">
                    <span class="fa-stack checkbox-button">
                        <i class="mdi mdi-check active">
                        </i>
                    </span>
                        {{$option}}
                    </label>
                    </input>
                </li>
                    @endforeach





            </ul>

        </div>

        <br>
        @endforeach

    </div>
</div>